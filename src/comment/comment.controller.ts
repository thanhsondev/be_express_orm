import { Body, Controller, Get, HttpCode, InternalServerErrorException, Param, Post, Req } from '@nestjs/common';
import { CommentService } from './comment.service';
import {ApiTags} from '@nestjs/swagger'
import { CommentIMG } from './dto/comment.dto';


@ApiTags("Comment")
@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

 
  @HttpCode(200)
  @Get("/get-comment/:id")
  getComment(@Param("id") id: string){
    try {
      let data = this.commentService.getDataComment(id)
      return data
      
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
    
  }

  @HttpCode(200)
  @Post("/comment-img")
  commentImage(@Body() body:CommentIMG){
    try {
      let data = this.commentService.commentImg(body)
      return data
      
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

}
