import { Injectable, Param, Req } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { CommentIMG } from './dto/comment.dto';

@Injectable()
export class CommentService {
    prisma = new PrismaClient()

   
     async getDataComment(@Param("id") id: string){

       let dataComment = await this.prisma.image_comment.findFirst({
            where: {
                image_id: Number(id)
            }
        })
        return dataComment
    }

    async commentImg(
        infoUser: CommentIMG
    ){
        let {image_id, user_id, content} = infoUser

        let newComment = {
            date_creat: new Date(),
            content: content,
            user_id:user_id,
            image_id: image_id
        }

        let data = this.prisma.image_comment.create({data:newComment})
        return data
    }
}
