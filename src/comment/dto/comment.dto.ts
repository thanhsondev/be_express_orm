import { ApiProperty } from "@nestjs/swagger"

export class CommentIMG {
    @ApiProperty()
    date_creat: string
    @ApiProperty()
    content: string
    @ApiProperty()
    user_id: number
    @ApiProperty()
    image_id: number
    
}