import { Controller, Get, HttpCode, InternalServerErrorException, Param, Req } from '@nestjs/common';
import { UsersService } from './users.service';
import {ApiTags} from '@nestjs/swagger'


@ApiTags("Users")
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @HttpCode(200)
  @Get("/get-users")
  getDataUsers(){
    try {
      let data = this.usersService.getUser()
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Get("/get-saved-img-by-user/:id")
  getSavedImgByUser(@Param("id") id: string){
    try {
      let data = this.usersService.getSavedImg(id)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Get("/get-create-img-by-user/:id")
  getCreatedImgByUser(@Param("id") id: string){
    try {
      let data = this.usersService.getCreatedImg(id)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }


}
