import { Injectable, Param, Req } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class UsersService {

    prisma = new PrismaClient()
    getUser(){
        let data = this.prisma.users.findMany()
        return data
    }

    getSavedImg(@Param("id") id: string){
        let data = this.prisma.saved_images.findMany({
            where: {
                user_id: Number(id)
            },
            include: {
                image: true
            }

        })
        return data
    }

    getCreatedImg(@Param("id") id: string){
 
        let data = this.prisma.users.findFirst({
            where: {
                user_id: Number(id)
            },
            include: {
                created_images: true
            }
        })
        return data
    }
}
