import { Controller, Post, Body, HttpCode, Res, Req, InternalServerErrorException } from '@nestjs/common';
import { AuthService } from './auth.service';
import {ApiTags} from '@nestjs/swagger'
import { LoginType, RegisterType } from './dto/auth.dto';


@ApiTags("Auth")
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @HttpCode(200)
  @Post("/register")
  rigester(@Body() body: RegisterType, @Res() res,) {
    try {
      this.authService.register(body, res)
      return
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  } 

  @HttpCode(200)
  @Post("/login")
  login(
    @Body() body: LoginType, @Res() res) {
      try {
        this.authService.login(body, res)
        return
      } catch (error) {
        throw new InternalServerErrorException("Internet Server Error")
      }
    }
}
