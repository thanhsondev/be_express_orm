import { ApiProperty } from "@nestjs/swagger"

export class RegisterType {
    @ApiProperty()
    name: string

    @ApiProperty()
    email: string

    @ApiProperty()
    password: string
}

export class LoginType {
    @ApiProperty()
    email: string

    @ApiProperty()
    password: string
}