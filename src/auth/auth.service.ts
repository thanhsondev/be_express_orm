import { Injectable, Res, Req } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import {JwtService} from '@nestjs/jwt'
import { LoginType, RegisterType } from './dto/auth.dto';


@Injectable()
export class AuthService {
    constructor(
        private jwtServise: JwtService
    ){}

    prisma = new PrismaClient()
    
    async register(
        infoUser: RegisterType,
        @Res() res,
       ){
        
        let {email, name, password} = infoUser
        const checkUser = await this.prisma.users.findFirst({
            where: {
                email: email
            },
        })
    
        if(checkUser){
            res.json({messege: "Email đã tồn tại"})
            return 
        }

        let passCrypt = bcrypt.hashSync(password, 10)
        let data = {
            name: name,
            email: email,
            password: passCrypt
        }
        await this.prisma.users.create({data})
        res.json({messege: "Đăng ký thành công"})
     
    }

    async login(
        infoUser: LoginType,
        @Res() res,
    ) {
        
        let {email, password} = infoUser
        const checkUser = await this.prisma.users.findFirst({
            where: {
                email: email
            },
        })

        if(checkUser) {
            let checkPass = bcrypt.compareSync(password, checkUser.password)
            if(checkPass){
                let token = this.jwtServise.sign({data:checkUser}, {
                    expiresIn: "60d", secret: "BIMAT"
                })
                return token
            } else {
                res.json({message: "Mật khẩu không đúng"})
            }
        } else {
            res.json({message: "Email không đúng"})
        }
    }
}
