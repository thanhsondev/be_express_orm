import { Controller, Get, HttpCode, InternalServerErrorException, Req, Res , Delete, Param, UseGuards} from '@nestjs/common';
import { ImageService } from './image.service';
import {ApiTags} from '@nestjs/swagger'
import {AuthGuard} from '@nestjs/passport'
import { ConfigService } from '@nestjs/config';

@ApiTags("Image")
@Controller('image')
export class ImageController {
  constructor(
    private readonly imageService: ImageService,
    private configSerivce: ConfigService
    ) {}

  // @UseGuards(AuthGuard("jwt"))
  @HttpCode(200)
  @Get("/get-image")
  getImage(){
    try {
      let data = this.imageService.getImg()
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Get("/get-image-by-name/:id")
  getImageByName(@Param("id") id: string){
    try {
      let data = this.imageService.getImgByName(id)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Get("/get-info-image/:id")
  getInfoImageById(@Param("id") id: string){
    try {
      let data = this.imageService.getInfoImg(id)
      
      return data
    } catch (Error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }


  @HttpCode(200)
  @Get("/check-image-saved/:id")
  checkImg(@Param("id") id: string, @Res() res){
    try {
      let data = this.imageService.getCheckImageSaved( id, res)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Get("/get-image-saved-byid/:id")
  getImgSavedById(@Param("id") id: string){
    try {
      let data = this.imageService.getImgSaveId( id)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Get("/get-image-created-byid/:id")
  getImgCreatedById(@Param("id") id: string){
    try {
      let data = this.imageService.getImgCreatedById( id)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Delete("/delete-image/:id")
  deleteImage(@Param("id") id: string, @Res() res){
    try {
      this.imageService.deleteCreatedImg(id)
      res.json({messege: "Đã xóa thành công"})
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

}
