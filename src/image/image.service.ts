import { Injectable, Req, Res, Param } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';


@Injectable()
export class ImageService {

    prisma = new PrismaClient()
   
    async getImg(){
        let dataImg = await this.prisma.images.findMany()
        return dataImg
    }

    async getImgByName(@Param("id") id: string){
        
        let dataImg = await this.prisma.images.findMany({
            where: {
                image_name: {
                    contains: id
                }
            }
        })
        return dataImg
    }


    async getInfoImg(@Param("id") id: string){
        
        let dataImg = await this.prisma.created_images.findFirst({
            where: {
                id: Number(id),
            },
            include: {
                users: true
            }

        })
        return dataImg
    }

    async getCheckImageSaved(
        @Param("id") id: string,
        @Res() res
    ){
        
        let data = await this.prisma.images.findFirst({
            where: {
                image_id: Number(id)
            }
        })
        if(data?.isSaved == true){
            res.json({message: "Hình đã được lưu"})
            return true
        } else if (data?.isSaved == false ){
            res.json({message: "Hình chưa được lưu"})
            return false
        }

    
    }

    async getImgSaveId (@Param("id") id: string) {
       
        let data = await this.prisma.saved_images.findMany({
            where: {
                user_id: Number(id)
            },
            include: {
                image: true
            }
        })
        return data
        
    }

    async getImgCreatedById (@Param("id") id: string) {
        let data = await this.prisma.created_images.findMany({
            where: {
                user_id: Number(id)
            },
        })
        return data
        
    }

    async deleteCreatedImg(@Param("id") id: string){
        
       await this.prisma.created_images.deleteMany({
        where: {
            image_id: Number(id)
        },
       })



       return
    }
}
