FROM node:18 

WORKDIR /usr/app 

COPY package*.json .

RUN yarn install 

COPY prisma ./prisma/
RUN yarn prisma generate

COPY . . 

EXPOSE 8080 

CMD ["yarn", "start"]

# docker build . -t img-node 

# docker run -d -p 8080:8080 --name cons-node video-node